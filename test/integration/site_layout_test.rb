require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end
  
  test "layout links" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", static_pages_home_path, count: 2
    assert_select "a[href=?]", static_pages_help_path
    assert_select "a[href=?]", static_pages_about_path
    assert_select "a[href=?]", static_pages_contact_path
    assert_select "a[href=?]", signup_path
    get static_pages_contact_path
    assert_select "title", full_title("Contact")
  end
  
  test "ユーザー一覧のリンク" do
    log_in_as(@user)
    get users_path

    assert_template 'users/index'
    assert_select "a[href=?]", users_path
  end

end